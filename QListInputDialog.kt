package ru.vstu.poignatov.decisiontheory

import android.app.DialogFragment
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import ru.vstu.poignatov.decisiontheory.util.findView

class QListInputDialog : DialogFragment() {
    private lateinit var q1View: EditText
    private lateinit var q2View: EditText
    private lateinit var q3View: EditText

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater?.inflate(R.layout.q_list_input_view, container, false)
                ?: super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view ?: return
        view.apply {
            val okButton: Button = findView(R.id.ok_button)
            okButton.setOnClickListener { onOkClicked() }
            q1View = findView(R.id.q1_value)
            q2View = findView(R.id.q2_value)
            q3View = findView(R.id.q3_value)
        }
    }

    private fun onOkClicked() {
        val q1 = q1View.text.toFloatOrToast() ?: return
        val q2 = q2View.text.toFloatOrToast() ?: return
        val q3 = q3View.text.toFloatOrToast() ?: return
        (activity as Callback).onQListEntered(listOf(q1, q2, q3))
        dismiss()
    }

    interface Callback {
        fun onQListEntered(qList: List<Float>)
    }

    private fun Editable.toFloatOrToast(): Float? {
        val floatValue = toString().toFloatOrNull()
        if (floatValue == null) {
            Toast.makeText(activity, R.string.InputDialog_BadFormat, Toast.LENGTH_SHORT).show()
        }
        return floatValue
    }

}