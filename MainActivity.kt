package ru.vstu.poignatov.decisiontheory

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import android.widget.Toast
import ru.vstu.poignatov.decisiontheory.algorithm.AlgorithmStrategy
import ru.vstu.poignatov.decisiontheory.algorithm.BayesLaplaceAlgorithm
import ru.vstu.poignatov.decisiontheory.algorithm.MinmaxAlgorithm
import ru.vstu.poignatov.decisiontheory.view.ResultViewWrapper
import ru.vstu.poignatov.decisiontheory.view.ResultViewWrapperImpl
import ru.vstu.poignatov.decisiontheory.view.TableViewWrapper
import ru.vstu.poignatov.decisiontheory.view.TableViewWrapperImpl

class MainActivity : AppCompatActivity(), QListInputDialog.Callback {
    private lateinit var tableViewWrapper: TableViewWrapper
    private lateinit var resultViewWrapper: ResultViewWrapper

    //Событие создания интерфейса
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Назначение разметки из которой будет создан интерфейс
        setContentView(R.layout.activity_main)
        //Создание обертки для получения массива введенных данных из интерфейса
        tableViewWrapper = TableViewWrapperImpl(findViewById(R.id.table) as ViewGroup)
        //Создание обертки для отображения результата и процесса обработки данных
        resultViewWrapper = ResultViewWrapperImpl(findViewById(R.id.result))

        //Обработка клика на кнопку "Минмакс"
        findViewById(R.id.minmax_button).setOnClickListener {
            //Запуск алгоритма "Минмакс"
            runAlgorithm(MinmaxAlgorithm())
        }
        //Обработка клина на кнопку "Байес - Лаплас"
        findViewById(R.id.bayes_laplace_button).setOnClickListener {
            //Отображение диалога ввода q массива
            QListInputDialog().show(fragmentManager, "")
        }
    }

    override fun onQListEntered(qList: List<Float>) {
        //Проверка суммы q, должна быть равна единице
        if (qList.sum() == 1f) {
            //Запуск алгоритма "Байес - Лаплас"
            runAlgorithm(BayesLaplaceAlgorithm(qList))
        } else {
            //В ином случае сообщаем пользователю об ошибке
            Toast.makeText(this, R.string.InputDialog_BadFormat, Toast.LENGTH_SHORT).show()
        }
    }

    //Запуск работы алгоритма и отображение результата и процесса выполнения
    private fun runAlgorithm(algorithmStrategy: AlgorithmStrategy) {
        //Получение введенных данных из таблицы
        val arguments = tableViewWrapper.getArguments()
        //Расчет результата выполнения алгоритма
        val result = algorithmStrategy.process(arguments)
        //Отображение результата и процесса выполнения
        resultViewWrapper.showResult(result)
    }

}
