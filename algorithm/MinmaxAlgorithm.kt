package ru.vstu.poignatov.decisiontheory.algorithm

import ru.vstu.poignatov.decisiontheory.util.inColumn

class MinmaxAlgorithm : AlgorithmStrategy {

    //Выполнение алгоритма
    override fun process(arguments: List<List<Float>>): AlgorithmStrategy.Result {
        //Определение минимума для каждой строки
        val subColumn = arguments.map { it.min() }
                .filterNotNull()
        //Определение максимума дополнительного столбца
        return subColumn.max()
                //Составление результат
                ?.let { Result(subColumn, it) }
                ?: AlgorithmStrategy.ErrorResult(IllegalArgumentException("Min or max value not found"))
    }


    class Result(
            private val subColumn: List<Float>,
            private val value: Float
    ) : AlgorithmStrategy.Result {

        //Преобразование результата в строку
        override fun getResult(): String = "$value"

        //Преобразование процесса вычисления в строку
        override fun getProcessing(): String? =
                //Складывание доп. столбца по-элементно один под другим
                subColumn.map { it.toString() }.inColumn()
    }

}