package ru.vstu.poignatov.decisiontheory.algorithm

import ru.vstu.poignatov.decisiontheory.algorithm.AlgorithmStrategy.ErrorResult
import ru.vstu.poignatov.decisiontheory.util.inColumn
import ru.vstu.poignatov.decisiontheory.util.inRow

class BayesLaplaceAlgorithm(private val qList: List<Float>) : AlgorithmStrategy {

    //Выполнение алгоритма
    override fun process(arguments: List<List<Float>>): AlgorithmStrategy.Result {
        //Составление дополнительного столбца
        val subColumn = arguments.mapIndexed { _, row ->
            //Обход каждой строки
            row.mapIndexed { index, element ->
                //Умножение элемента строки на q с тем же инлексом
                element * qList[index]
            }
                    //Суммирование элементов строки
                    .sum()
        }.filterNotNull()

        //Вычисление максимума дополнительного столбца
        return subColumn.max()
                //Составление результат
                ?.let { Result(qList, subColumn, it) }
                ?: ErrorResult(IllegalArgumentException("Max value not found"))
    }

    class Result(
            private val qArray: List<Float>,
            private val subColumn: List<Float>,
            private val value: Float
    ) : AlgorithmStrategy.Result {

        //Преобразование результата в строку
        override fun getResult(): String = "$value"

        //Преобразование процесса вычисления в строку
        override fun getProcessing(): String? {
            //Составление строки для отображения текущего массива q
            val qArrayText = qArray.mapIndexed { index, q -> "q${index + 1} = $q " }.inRow()
            //Складывание доп. столбца по-элементно один под другим
            val subColumnText = subColumn.map { it.toString() }.inColumn()
            return qArrayText + "\n" + subColumnText
        }

    }

}