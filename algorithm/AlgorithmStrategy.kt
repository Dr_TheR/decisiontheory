package ru.vstu.poignatov.decisiontheory.algorithm

interface AlgorithmStrategy {

    fun process(arguments: List<List<Float>>): Result

    interface Result {

        fun getResult(): String
        fun getProcessing(): String?

    }

    class ErrorResult(private val error: Exception) : Result {

        override fun getResult(): String = error.message ?: ""

        override fun getProcessing(): String? = null
    }

}