package ru.vstu.poignatov.decisiontheory.view

import android.support.annotation.IdRes
import android.view.ViewGroup
import android.widget.EditText
import ru.vstu.poignatov.decisiontheory.R
import ru.vstu.poignatov.decisiontheory.util.findView

interface TableViewWrapper {

    fun getArguments(): List<List<Float>>

}

class TableViewWrapperImpl(private val rootView: ViewGroup): TableViewWrapper {
    private val table: List<List<EditText>> = listOf(
            listOf(editText(R.id.R1C1), editText(R.id.R1C2), editText(R.id.R1C3)),
            listOf(editText(R.id.R2C1), editText(R.id.R2C2), editText(R.id.R2C3)),
            listOf(editText(R.id.R3C1), editText(R.id.R3C2), editText(R.id.R3C3))
    )

    private fun editText(@IdRes id: Int): EditText = rootView.findView(id)

    override fun getArguments(): List<List<Float>> =
            table.map { row->
                row.map { editText ->
                    editText.text.toString().toFloatOrNull() ?: 0f
                }
            }
}