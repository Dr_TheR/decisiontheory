package ru.vstu.poignatov.decisiontheory.view

interface ResultViewWrapper {

    fun showResult(result: ru.vstu.poignatov.decisiontheory.algorithm.AlgorithmStrategy.Result)

}

class ResultViewWrapperImpl(view: android.view.View) : ru.vstu.poignatov.decisiontheory.view.ResultViewWrapper {
    val resultView: android.widget.TextView = view as android.widget.TextView
    val context: android.content.Context = resultView.context

    override fun showResult(result: ru.vstu.poignatov.decisiontheory.algorithm.AlgorithmStrategy.Result) {
        val resultValue = context.getString(ru.vstu.poignatov.decisiontheory.R.string.Algorithm_Result, result.getResult())
        val processingValue = result.getProcessing()
                ?.let { context.getString(ru.vstu.poignatov.decisiontheory.R.string.Algorithm_Processing, it) } ?: ""
        resultView.text = processingValue + "\n\n" + resultValue
    }
}