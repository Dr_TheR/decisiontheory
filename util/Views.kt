package ru.vstu.poignatov.decisiontheory.util

import android.support.annotation.IdRes
import android.view.View

@Suppress("UNCHECKED_CAST")
fun <T : View> View.findView(@IdRes id: Int): T =
        findViewById(id) as T