package ru.vstu.poignatov.decisiontheory.util

fun List<String>.inColumn() = joinToString("\n")

fun List<String>.inRow() = joinToString("  ")